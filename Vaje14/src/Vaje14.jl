module Vaje14

using LinearAlgebra

export ZacetniProblemNDE, ParamDOPRI5, ParamMetodeRK2, resi

"""
    ZacetniProblemNDE((t0, tk), y0, p, f)

Podatkovni tip za opis začetnega problema za navadno diferencialno enačbo (NDE)
y'(t) = f(t, y(t)) z začetnim pogojem y(t0) = y0. 
"""
struct ZacetniProblemNDE
    int_t # interval za t med začetnim t0 in končnim tn
    y0 # začetni pogoj y(t0) (vrednost rešitve v int_t[1])
    p # parametri, ki nastopajo v diferencialni enčbi
    f # funkcija desnih strani DE y'(t) = f(t, y(t), p)
end

"""
    ResitevNDE(t, y, problem::ZacetniProblemNDE)

Podatkovni tip, ki predstavlja izračunane približke `y` v točkah `t` za začetni
problem za NDE na intervalu podanem z začetnim problemom `problem`
"""
struct ResitevNDE
    t # tabela t-jev, v katerih so izračunani približki
    y # tabela približkov za rešitev y[i] ≈ y(t[i])
    problem::ZacetniProblemNDE # problem, katerega rešitev je
end

"""
    param = ParamMetodeRK2(n)

Parametri za metodo Runge-Kutta reda 2 s fiksnim korakom:
- `n` število korakov 
"""
struct ParamMetodeRK2
    n::Integer # število korakov metode
end

"""
    resitev = resi(problem::ZacetniProblemNDE, param::ParamMetodeRK2)

Poišči približek za rešitev začetnega problema za NDE z metodo Runge-Kutta reda 2 s 
fiksnim korakom.
"""
function resi(problem::ZacetniProblemNDE, param::ParamMetodeRK2)
    t0, tk = problem.int_t
    y0 = problem.y0
    dim = length(y0)
    f = problem.f
    p = problem.p
    n = param.n
    t = LinRange(t0, tk, n+1)
    h = t[2] - t[1]
    y = zeros(dim, n+1)
    y[:, 1] = y0
    for i=1:n
        k1 = h*f(t[i], y[:,i], p)
        k2 = h*f(t[i+1], y[:, i] + k1, p)
        y[:, i+1] = y[:, i] + (k1 + k2)/2
    end
    return ResitevNDE(t, y, problem)
end


# Bazne funkcije za [Hermitov kubični zlepek](https://en.wikipedia.org/wiki/Cubic_Hermite_spline). 
h00(t) = (1+2t)*(1-t)^2
h10(t) = t*(1-t)^2
h01(t) = t^2*(3-2t)
h11(t) = t^2*(t-1)

"""
    y = vrednost(x, res::ResitevNDE)

Izračunaj vrednost rešitve navadne diferencialne enačbe v dani točki `x`.
"""
function vrednost(x, res::ResitevNDE)
    t = res.t
    if (x < t[1])|| (x > t[end])
        throw("Vrednost x ni v definicijskem območju")
    end
    indeks = max(searchsortedfirst(t, x) - 1, 1)
    x0 = t[indeks]
    x1 = t[indeks + 1]
    y0 = res.y[:, indeks]
    y1 = res.y[:, indeks + 1]
    f = res.problem.f
    m0 = f(x0, y0, res.problem.p)
    m1 = f(x1, y1, res.problem.p)
    t = (x -x0)/(x1 - x0)
    return h00(t)*y0 + h10(t)*(x1 - x0)*m0 + h01(t)*y1 + h11(t)*(x1 - x0)*m1
end

"""
Definiraj, da se podatkovni tip ResitevNDE obnaša kot funkcija.
Glej [podatkovni tipi kot funkcije](https://docs.julialang.org/en/v1/manual/methods/#Function-like-objects)
# Primer

```julia
zac_problem = ZacetniProblemNDE((x, y) -> x*y, 0.1, [0, 1])
resitev = resi(zac_problem)
resitev(0.2)
```
"""
function (res::ResitevNDE)(x)
    vrednost(x, res)
end

# Iskanje ničel rešitve začetnega problema za NDE

"""
    resitev = resi(problem::ZacetniProblemNDE, g, grad_g, param::ParamMetodeRK2)

Poišči rešitev začetnega problema za NDE `problem`, pri čemer je končna vrednost neodvisne 
spremenljivke `tk` izbrana tako, da je `g(y(tk))` enak 0. Uporabi metodo Runge-Kutta reda 2.
"""
function resi(problem::ZacetniProblemNDE, g, grad_g, param::ParamMetodeRK2)
    t0, tk = problem.int_t
    y0 = problem.y0
    dim = length(y0)
    f = problem.f
    p = problem.p
    h = (tk - t0)/param.n
    tab_t = [t0]
    tab_y = y0
    g0 = g(y0)
    for i=1:param.n
        # nov približek z RK2
        y1 = korak_RK2(f, tab_t[i], tab_y[:,i], p, h)
        g1 = g(y1)
        if g0 * g1  < 0
            t1, y1 = newton_RK2(f, tab_t[i], tab_y[:, i], p, g, grad_g)
            tab_y = hcat(tab_y, y1)
            tab_t = hcat(tab_t, t1)
            return ResitevNDE(tab_t, tab_y, problem)
        end
        tab_y = hcat(tab_y, y1)
        tab_t = hcat(tab_t, tab_t[i] + h)
    end
    trow("Rešitev NDE nima ničle")
end    

function newton_RK2(f, t0, y0, p, g, dg; ε = 1e-10)
    g0 = g(y0)
    if abs(g0) < ε
        return t0, y0
    end
    h = - g(y0)/(dot(dg(y0), f(t0, y0, p)))
    t1 = t0 + h
    y1 = korak_RK2(f, t0, y0, p, h)
    newton_RK2(f, t1, y1, p, g, dg; ε = ε)
end

function korak_RK2(f, t0, y0, p, h)
    k1 = h*f(t0, y0, p)
    k2 = h*f(t0 + h, y0 + k1, p)
    y0 + (k1 + k2)/2
end

struct ParamDOPRI5
    ε
    h0
    σ
end

"""
    resitev = resi(problem::ZacetniProblemNDE, param::ParamDOPRI5)

Poišči približek za rešitev začetnega problema za NDE podanega v `problem` z metodo
DOPRI5 s parametetri `param`.

# Primer

```julia
# Van der Polov oscilator
f(t, y, mu) = [y[2], μ*(1 - y[1]^2)*y[2] - y[1]]
problem = ZacetniProblemNDE((0, 10), [0, 1], 0.5, f)
param = ParamDOPRI5(1e-8, 0.01, 0.9)
res = resi(problem, param)
scatter(res.t, res.y)
plot(t -> res(t), ...res.problem.int_t)
```
"""
function resi(problem::ZacetniProblemNDE, param::ParamDOPRI5)
    ε = param.ε
    h = param.h0
    σ = param.σ
    t, b = problem.int_t
    y = problem.y0
    tab_t = [t]
    tab_y = y
    f = problem.f
    p = problem.p
    while t < b
        k1 = h*f(t, y, p)
        k2 = h*f(t+h/5, y + k1/5, p)
        k3 = h*f(t+h*0.3, y + k1*0.075 + k2*0.225, p)
        k4 = h*f(t+h*0.8, y+44*k1/45-56*k2/15+32*k3/9, p)
        k5 = h*f(t+8*h/9, y+19372*k1/6561-25360*k2/2187+64448*k3/6561-212*k4/729, p)
        k6 = h*f(t+h, y+9017*k1/3168-355*k2/33+46732*k3/5247+49*k4/176-5103*k5/18656, p)
        k7 = h*f(t+h, y+35*k1/384+500*k3/1113+125*k4/192-2187*k5/6784+11*k6/84, p)
        le = 71*k1/57600-71*k3/16695+71*k4/1920-17253*k5/339200+22*k6/525-k7/40;
        nle = norm(le);
        if(nle<ε*h)
           y = y + (5179*k1/57600+7571*k3/16695+393*k4/640-92097*k5/339200+187*k6/2100+k7/40);
           t = t+h;                                                             
           tab_t = vcat(tab_t, t)
           tab_y = hcat(tab_y, y)
           h = h*σ*(ε*h/nle)^(1/5)
           if t+h > b
              h = b-t
           end
        else
           h = h/2
        end
     end
     ResitevNDE(tab_t, tab_y, problem) 
end
end # module
