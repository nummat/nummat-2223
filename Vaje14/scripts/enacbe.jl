using Vaje14

# Poišči vrednost t, ko nihalo doseže ravnovesno lego
# Dušeno vsiljeno nihanje
function nihanje(t, Y, p)
    β, ω0, A, ω1 = p
    y, z = Y
    return [z, A*sin(ω1*t) - β*z - ω0^2*y]
end 

problem = ZacetniProblemNDE([0, 5], [1, 0], [0.1, 1, 2, 5], nihanje)
g(y) = y[1]
grad_g(y) = [1, 0]
res = resi(problem, g, grad_g, ParamMetodeRK2(500))
res.t[end]
res.y[:,end]
plot(res.t', res.y')