using Vaje14

# Primer
# Dušeno vsiljeno nihanje
function nihanje(t, Y, p)
    β, ω0, A, ω1 = p
    y, z = Y
    return [z, A*sin(ω1*t) - β*z - ω0^2*y]
end

problem = ZacetniProblemNDE([0, 20], [1, 0], [0.1, 1, 2, 5], nihanje)
resitev = resi(problem, ParamMetodeRK2(500))
using Plots
plot(resitev.t, resitev.y')

resitev(5.3123)

plot(t -> resitev(t)[1], 4.5, 15.5)