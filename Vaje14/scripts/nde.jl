using Vaje14

# Vzbujeno harmonično nihalo
# y'' + βy' + ω0^2y= sin(ω1t)
function nihalo(t, z, p)
    β, ω0, ω1 = p
    y, dy = z
    return [dy, sin(ω1*t)-β*dy - ω0^2 * y]
end

problem = ZacetniProblemNDE([0, 20.0], [1., 0.], [0.1, 1., 2.], nihalo)
resitev = resi(problem, ParamMetodeRK2(1000))
plot(resitev.t, resitev.y')
# fazni portret
plot(resitev.y[1,:], resitev.y[2,:])

# Poiščimo, kdaj pride nihalo v ravnovesno lego
g(z) = z[1] # zanima nas ničla položaja, ki je prva komponenta vektorja rešitve 
dg(z) = [1, 0]

resitev = resi(problem, g, dg, ParamMetodeRK2(1000))
resitev.t[end]
resitev.y[:, end]
plot(resitev.t, resitev.y')

# Reši NDE enačbo za Van der Polov oscilator
# y'' - μ(1 - x^2)y' + x = 0

f(t, y, μ) = [y[2], μ*(1 - y[1]^2)*y[2] - y[1]]
problem = ZacetniProblemNDE((0, 10), [0, 1], 1.5, f)

# Metoda RK2

paramRK2 = ParamMetodeRK2(100)
res = resi(problem, paramRK2)

# resitev lahko uporabimo kot funkcijo
res(5)

t0, tk = problem.int_t

using Plots

scatter(res.t, res.y')
plot!(t -> res(t)[1], t0, tk)
plot!(t -> res(t)[2], t0, tk)

# fazni portret
scatter([problem.y0[1]], [problem.y0[2]])
plot!(res.y[1, :], res.y[2, :])


# metoda s kontrolo koraka
param = ParamDOPRI5(1e-3, 0.01, 0.9)
res = resi(problem, param)

res(5)

using Plots

scatter(res.t, res.y')
t0, tk = res.problem.int_t
plot!(t -> res(t)[1], t0, tk)
plot!(t -> res(t)[2], t0, tk)