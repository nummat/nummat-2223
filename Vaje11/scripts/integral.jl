# Izračunaj integral od sin(x) na [0, 1]
# prava vrednost
I_p = cos(0) - cos(1)
# s trapezno formulo
a = 0
b = 1
I_t = (b - a)/2 * (sin(a) + sin(b))
I_t - I_p

# s Simpsonovo formulo
h = (b -a)
I_s = h/6*(sin(a) + 4*sin(a+h/2) + sin(a+h))
I_s - I_p

# s sestavljeno Simpsonovo formulo
"""
    I = integral_simpson(fun, a, b, n)

Izračunaj integral funkcije `fun` na intervalu `[a, b]` s sestavljenim Simpsonovim pravilom
z `n` podintervali.
"""
function integral_simpson(fun, a, b, n)
    h = (b - a) / n
    utezi = vcat([1, 4], [[2, 4] for i=1:n-1]..., [1])
    vozli = LinRange(a, b, 2n + 1)
    return sum(utezi.*fun.(vozli)) * h / 6
end

integral_simpson(sin, 0, 1, 1) - I_s

# odvisnost napake od števila podintervalov
function napaka_simpson(intervali)
    I = [integral_simpson(sin, 0, 1, i) for i in intervali]
    I .- I_p
end

using Plots

scatter(log10.(1:100), log10.(abs.(napaka_simpson(1:100))))

# Integral sin s 5/9 pravilom
x_1 = sqrt(3/5)

vozli = [-x_1, 0, x_1]
vozli_ab(a, b, vozli) = (b-a)/2*vozli .+ (a + b)/2
vozli_ab(0, 1)

utezi = [5, 8, 5]./9
utezi_ab(a, b, utezi) = (b-a)/2*utezi
utezi_ab(0, 1)

I_59 = sum(utezi_ab(0, 1).*sin.(vozli_ab(0, 1)))
I_59 - I_p

# Gauss-Legendrova formula za n=8
utezi_8 = [0.3626837833783620 	  ,	0.3626837833783620,	0.3137066458778873 ,0.3137066458778873,	0.2223810344533745, 0.2223810344533745,	0.1012285362903763 ,	0.1012285362903763
] 

vozli_8 = [-0.1834346424956498, 0.1834346424956498, -0.5255324099163290, 0.5255324099163290, -0.7966664774136267,
            0.7966664774136267, -0.9602898564975363, 0.9602898564975363]
I_gl8 = sum(utezi_ab(0, 1, utezi_8).*sin.(vozli_ab(0, 1, vozli_8)))
I_gl8 - I_p

# Gauss-Legendrove kvadrature
x0, y0 