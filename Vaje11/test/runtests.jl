using Test, Vaje11

@testset "Gauss Legendrove kvadrature" begin
    x0, w = gauss_legendre_rule(3)
    p = sortperm(x0)
    x0 = x0[p]
    w = w[p]
    @test x0 ≈ [-sqrt(3/5), 0, sqrt(3/5)]
    @test w ≈ [5, 8, 5]./9
end

@testset "Transformacija kvadratur" begin
    x0 = [0, 1, 2]
    w = [1, 2, 3]
    x0_nov, w_nov = transform_rule(x0, w, (0, 2), (2, 5))
    @test x0_nov ≈ [2, 3.5, 5]
    @test w_nov ≈ 3/2*w
end 