module Vaje11

using LinearAlgebra

export gauss_quad_rule, gauss_legendre_rule, transform_rule

"""
    x, w = gauss_quad_rule(a, b, c, mu, n)

Izračuna uteži `w` in vozlišča `x` za 
[Gaussove kvadraturne formule](https://en.wikipedia.org/wiki/Gaussian_quadrature) 
za integral

```math
\\int_a^b f(x)w(x)dx \\simeq w_1f(x_1)+\\ldots w_n f(x_n)
```

z Golub-Welshovim algoritmom.

Parametri `a`, `b` in `c` so funkcije `n`, ki določajo koeficiente v tročlenski 
rekurzivni formuli za ortogonalne polinome na intervalu ``[a,b]`` z utežjo `w(x)`

```math
p_n(x) = (a(n)x+b(n))p_{n-1}(x) - c_n p_{n-2}(x)
```

`mu` je vrednost integrala uteži na izbranem intervalu

```math
\\mu = \\int_a^b w(x)dx
```

# Primer
za računanje integrala z utežjo ``w(x)=1`` na intervalu ``[-1,1]``, lahko uporabimo
[Legendrove ortogonalne polinome](https://sl.wikipedia.org/wiki/Legendrovi_polinomi), 
ki zadoščajo rekurzivni zvezi 

```math
p_{n+1}(x) = \\frac{2n+1}{n+1}x p_n(x) -\\frac{n}{n+1} p_{n-1}
```

Naslednji program izpiše vozlišča in uteži za n od 1 do 5

```julia
a(n) = (2*n-1)/n; b(n) = 0.0; c(n) = (n-1)/n;
μ = 2;
println("Gauss-Legendrove kvadrature")
for n=1:5
  x0, w = gauss_quad_rule(a, b, c, μ, n);
  println("n=\$n")
  println("vozlišča: ", x0)
  println("uteži: ", w)
end
```
"""
function gauss_quad_rule(a, b, c, mu, n)
  d = zeros(n)
  du = zeros(n-1)
  d[1] = -b(1)/a(1)
  for i=2:n
    d[i] = -b(i)/a(i)
    du[i-1] = sqrt(c(i)/(a(i-1)*a(i)))
  end
  J = SymTridiagonal(d, du)
  F = eigen(J)
  x0 = eigvals(F)
  w = eigvecs(F)[1,:].^2*mu
  return x0, w
end
"""
    x0, w = gauss_legendre_rule(n)

Izračunaj vozle in uteži za Gauss-Legendrovo kvadraturo z `n` vozlišči 
na intervalu [-1, 1]. Gauss-Legendrovo kvadraturo približno oceni vrednost integrala
funckije `f(x)` na intervalu [-1, 1].

# Primer

```julia
x0, w = gauss_legendre_rule(5)
f(x) = cos(x)
I = sum(f.(x0).*w) # Približek za integral cos na [-1, 1]
```
"""
function gauss_legendre_rule(n)
    a(n) = (2*n-1)/n;
    b(n) = 0.0;
    c(n) = (n-1)/n;
    μ = 2;
    x0, w = gauss_quad_rule(a, b, c, μ, n);
    return x0, w
end

"""
    x0_nov, w_nov = transform_rule(x0, w, (a0, b0), (a1, b1))

Pretvori uteži za kvadraturo na intervalu [a0, b0] na interval [a1, b1].

# Primer

```julia
# Gauss-Legendre na 3 točkah
x0 = [-sqrt(3/5), 0, sqrt(3/5)]
w = [5, 8, 5]./9
x0_nov, w_nov = transform_rule(x0, w, (-1, 1), (2, 3))
```
"""
function transform_rule(x0, w, (a0, b0), (a1, b1))
    k = (b1 - a1)/(b0 - a0)
    n = b1 - k*b0
    vozli = k*x0 .+ n
    utezi = k*w
    return vozli, utezi
end

end # module
