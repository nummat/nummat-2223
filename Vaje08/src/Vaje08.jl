module Vaje08

using  LinearAlgebra

export konvergencno_obmocje, newton, gradientni_spust

"""
    x, it = newton(F, DF, x0; maxit=maxit, tol=tol)

Poišči približek za rešitev enačbe F(x)=0 z Newtonovo metodo za dano 
funkcijo `F`, odvodom funkcije `DF` in začetnim približkom x0
"""
function newton(F, DF, x0; maxit=100, tol=1e-10)
    for i=1:maxit
        z = F(x0)
        x = x0 - DF(x0)\z # x0 - DF^(-1)*z
        if norm(z, Inf) < tol
            return x, i
        end
        x0 = x
    end
    throw("Newtonova metoda ne konvergira")
end

"""
    x, it = gradientni_spust(grad, x0; h=0.1, tol=1e-5, maxit=100)
"""
function gradientni_spust(grad, x0; h=0.1, tol=1e-5, maxit=1000)
    for i=1:maxit
        grad_x = grad(x0)
        x = x0 - h*grad_x
        if norm(grad_x) < tol
            return x, i
        end
        x0 = x
    end
    throw("Gradientni spust ne konvergira!")
end

"""
    x, y, Z = konvergencno_obmocje(obmocje, metoda; n=50, m=50, maxit=50, tol=1e-3)

Izračuna, h katerim vrednostim konvergira metoda `metoda`, če uporabimo različne
začetne približke.

# Primer
Konvergenčno območje za Newtonovo metodo za kompleksno enačbo ``z^3=1``

```jldoctest
julia> F((x, y)) = [x^3-3x*y^2; 3x^2*y-y^3];
julia> JF((x, y)) = [3x^2-3y^2 -6x*y; 6x*y 3x^2-3y^2]
julia> metoda(x0) = newton(F, JF, x0; maxit=10; tol=1e-3);

julia> x, y, Z = konvergencno_obmocje((-2,2,-2,2), metoda; n=5, m=5); Z
5×5 Array{Float64,2}:
 1.0  1.0  2.0  3.0  3.0
 1.0  1.0  2.0  3.0  3.0
 1.0  1.0  0.0  3.0  3.0
 2.0  2.0  2.0  2.0  2.0
 2.0  2.0  2.0  2.0  2.0
```
"""
function konvergencno_obmocje(obmocje, metoda; n=50, m=50, tol=1e-3)
    a, b, c, d = obmocje
    Z = zeros(n, m)
    x = LinRange(a, b, n)
    y = LinRange(c, d, m)
    nicle = []
    for i = 1:n, j = 1:m
        z = [x[i], y[j]]
        try
            z, it = metoda([x[i],y[j]])
        catch
            continue
        end
        if z === nothing
          continue
        end
        k = findfirst([norm(z - z0, Inf) < 2tol  for z0 in nicle])
        if k === nothing
          push!(nicle, z)
          k = length(nicle)
        end
        Z[i,j] = k
    end
    return x, y, Z
end


end # module