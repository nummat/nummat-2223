using Plots
using Vaje08
# koordinatne funkcije za Lissajoujevo krivoljo
x(t) = sin(2t)
y(t) = cos(3t)

# graf krivulje
t = LinRange(0, 2π, 100)
plot(x.(t), y.(t))

# poiščemo samopresečišča z Newtonovo metodo
F(t, s) = [x(t) - x(s), y(t) - y(s)]
dx(t) = 2cos(2t)
dy(t) = -3sin(3t)
JF(t, s) = [dx(t) -dx(s); dy(t) -dy(s)]
F(2, 3)
JF(2, 3)

# Reševnje sistem enačb kot minimum norme funkcije desnih strani
contourf(t, t, (t,s) -> norm(F(t, s)))
title!("Norma F(t, s). Rešitve so tam, kjer je norma 0.")

# Newtonova metoda "na roke"
ts0 = [0.1, 5.0]
ts = ts0 - JF(ts0...)\F(ts0...); ts0 = ts
F(ts...)

scatter!([x(ts[1])], [y(ts[1])])

# Newtonova metoda s funkcijo
ts0 = [0.1, 5.0]
F(ts) = F(ts...)
JF(ts) = JF(ts...)
ts, it = Vaje08.newton(F, JF, ts0)
scatter!([x(ts[1])], [y(ts[1])])

# Drugo presečišče
ts0 = [0.4, 3.0]
ts, it = Vaje08.newton(F, JF, ts0)
scatter!([x(ts[1])], [y(ts[1])])

# konvergenčno območje
metoda(ts0) = Vaje08.newton(F, JF, ts0)
t, s, Z = konvergencno_obmocje((0, 2π, 0, 2π), metoda; n=200, m=200)
heatmap(t, s, Z)