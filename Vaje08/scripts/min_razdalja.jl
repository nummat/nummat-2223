using Vaje08
using Plots

# Definiramo parametrično podani krivulji
K1(t) = [2cos(t) + 1/3, sin(t) + 1/4]
K2(s) = [cos(s)/3 - sin(s)/2, cos(s)/3 + sin(s)/2]

# Narišemo obe krivulji
"""
Spremeni točko kot vektor v točko kot n-terico koordinat.
Funkcijo potrebujemo, če želimo točko narisati na graf. 
"""
tocka(x) = tuple(x...) 
t = LinRange(0, 2π, 100)
plot(tocka.(K1.(t)), label="K1")
plot!(tocka.(K2.(t)), label="K2")

using LinearAlgebra
# Razdalja med točkama na krivuljah
d(t, s) = norm(K1(t) - K2(s))
K1(0) - K2(0)
d(0, 0)

# Graf funkcije razdalje
contourf(t, t, d)

# Iskanje minima z gradientno metodo

# Za izračun gradientov, bomo uporabili avtomatsko odvajanje.
using ForwardDiff

# Funkcija razdalje kot funkcija vektorskega argumenta
d(x) = d(x...)
grad_d(x) = ForwardDiff.gradient(d, x)
grad_d([1,2])

"""
    simuliraj_spust(;n=n)

Simuliraj `n` korakov gradientnega spusta za iskanje minimalne razdalje in grafično
predstavi zaporedje približkov.
"""
function simuliraj_spust(;n=100)
    t = LinRange(-π, π, 100)
    graf = contourf(t, t, d)
    ts0 = [0.0, 0.0]
    h = 0.1
    priblizki = [tocka(ts0)]
    for i=1:n
        # gradientni spust
        ts = ts0 - h*grad_d(ts0)
        ts0 = ts
        append!(priblizki, [tocka(ts)])
    end
    graf = scatter!(graf, tocka.(priblizki)) 
end

simuliraj_spust(n=500)

# Iskanje stacionarnih točk z Newtonovo metodo
# Iščemmo ničle gradienta. Za Newtonovo metodo potrebujemo odvod gradienta, ki se 
# imenuje Hessian.
Hess(x) = ForwardDiff.hessian(d, x)
Hess([1,2])

# Približek za stacionarno točko

x, it = newton(grad_d, Hess, [1., 2.])

# Graf funkcije razdalje
contourf(t, t, d)
scatter!([x[1] + 2π], [x[2]])

x, it = newton(grad_d, Hess, [2., 4.])
scatter!([x[1]], [x[2]])

x, it = newton(grad_d, Hess, [3., 2.])
scatter!([x[1]], [x[2]])

# Območje konvergence za Newtonovo metodo
function metoda_newton(x)
   x, it = newton(grad_d, Hess, x)
   return [x[1] % π, x[2] % π]
end
x, y, Z = konvergencno_obmocje((-π, π, -π, π), metoda_newton; n=200, m=200)
heatmap(x, y, Z)

# Območje konvergence za gradientno metodo
x, it = gradientni_spust(grad_d, [1, 2])
# Graf funkcije razdalje
contourf(t, t, d)
scatter!([x[1]], [x[2]])

# konvergenčno območje za gradientni spust
function metoda_spust(x)
    x, it = gradientni_spust(grad_d, x)
    return [x[1] % π, x[2] % π]
 end
 x, y, Z = konvergencno_obmocje((-π, π, -π, π), metoda_spust; n=100, m=100)
 heatmap(x, y, Z)