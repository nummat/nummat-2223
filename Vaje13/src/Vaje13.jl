module Vaje13

export  ZacetniProblemNDE, resi_euler, resi_trapez

struct ZacetniProblemNDE
    f # funkcija desnih strani DE y' = f(x, y)
    x0 # začetna vrednost neodvisne spremenljivke
    y0 # začetna vrednost odvisne spremenljivke
end

"""
    y, x = resi_euler(zp::ZacetniProblemNDE, xk; n = n)

Reši začetni problem za NDE z Eulerjevo metodo na [zp.x0, xk] s fiksnim korakom.
Parameter `n` določa število korakov. Funkcija vrne dva vektorja z izračunanimi vrednostmi `y` in
vrednostmi neodvisne spremenljivke `x`.
"""
function resi_euler(zp::ZacetniProblemNDE, xk; n = 100)
    y = zeros(n+1, length(zp.y0))
    y[1,:] = zp.y0
    x = LinRange(zp.x0, xk, n+1)
    h = x[2] - x[1]
    for i=1:n
        y[i+1, :] = y[i, :] + h * zp.f(x[i], y[i, :]) 
    end
    return y, x
end

function resi_trapez(zp::ZacetniProblemNDE, xk; n = 100)
    y = zeros(n+1, length(zp.y0))
    y[1,:] = zp.y0
    x = LinRange(zp.x0, xk, n+1)
    h = x[2] - x[1]
    for i=1:n
        y[i+1, :] = y[i, :] + h * zp.f(x[i], y[i, :]) # Euler
        y[i+1, :] = y[i, :] + h / 2 * (zp.f(x[i], y[i, :]) + zp.f(x[i+1], y[i+1, :]))  
    end
    return y, x
end

end # module
