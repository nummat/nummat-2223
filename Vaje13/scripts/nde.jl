using Vaje13

# Začetni problem za logistično enačbo
logistik_f(x, y) = y*(1 - y)
logistik_zp = Vaje13.ZacetniProblemNDE(logistik_f, 0, 0.1)
# rešitev z Eulerjevo metodo
y, t = resi_euler(logistik_zp, 10)

using Plots

plot(t, y)

# uporabimo več korakov

y_1000, t_1000 = resi_euler(logistik_zp, 10; n=1000)

plot!(t_1000, y_1000)

# prava rešitev je podana z logistično funkcijo
logistik(t) = 1/9*exp(t)/(1+1/9*exp(t))

plot!(logistik, 0, 10)

# graf napake

plot(t, y - logistik.(t))
plot!(t_1000, y_1000 - logistik.(t_1000))

# Lotka Voltera
# x' = αx - βxy
# y' = δxy - γy
# z = [x, y], p = [α, β, γ, δ]
lotka_volterra(t, z, p) = [p[1]*z[1] - p[2]*z[1]*z[2], p[4]*z[1]*z[2] - p[3]*z[2]]

# primer
p = [1.1, 0.4, 0.1, 0.4]
x0 = 5.0
y0 = 1.0 

lv_zacetni_problem = Vaje13.ZacetniProblemNDE((t, z) -> lotka_volterra(t, z, p), 0.0, [x0, y0])

z, t = resi_euler(lv_zacetni_problem, 100; n=10000)
plot(t, z)

# rešitev v fazni ravnini
plot!(z[:,1], z[:,2])

# rešitev s trapezno metodo

z_tr, t_tr = resi_trapez(lv_zacetni_problem, 100; n=1000)
plot(z_tr[:,1], z_tr[:,2])