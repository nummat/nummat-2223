module Vaje03

using SparseArrays

export LaplaceovOperator, matrika, desne_strani, RobniProblemPravokotnik, resi

"""
    L = LaplaceovOperator{2}()

Podatkovni tip brez vrednosti, ki predstavlj laplaceov operator v 
d dimenzijah.
"""
struct LaplaceovOperator{d} end

"""
    LaplaceovOperator(d)

Vrni vrednost tipa LaplaceovOperator{d} v `d` dimenzijah.
"""
LaplaceovOperator(d) = LaplaceovOperator{d}()

"Izračuanj indeks v matriki sistema za element (i,j) v matriki."
indeks(i, j, n) = (j-1)*n + i

"""
    L = matrika(n, m, LaplaceovOperator(2))

Izračunaj matriko za Laplaceov operator v 2D za pravokotno mrežo dimenzij `n x m`.
"""
function matrika(n, m, ::LaplaceovOperator{2})
    N = n * m
    A = spzeros(N, N)
    for i = 1:n, j=1:m
        I = indeks(i, j, n) # indeks vrstice
        A[I, indeks(i, j, n)]  = -4 # diagonala
        # sosedi
        if (i-1 >=1)
            A[I, indeks(i-1, j, n)] = 1
        end
        if (j-1 >=1)
            A[I, indeks(i, j-1, n)] = 1
        end
        if (i+1 <=n)
            A[I, indeks(i+1, j, n)] = 1
        end
        if (j+1 <=m)
            A[I, indeks(i, j+1, n)] = 1
        end
    end
    return A
end

"""
    b = desne_strani(s, z, l, d, ::LaplaceovOperator{2})

Sestavi vektor desnih strani za linearni sistem za robni problem za Laplaceovo enačbo na
pravokotniku.
"""
function desne_strani(s, z, l, d, ::LaplaceovOperator{2})
    n = length(s)
    m = length(l)
    N = n * m # dimenzije sistema
    b = zeros(N)
    # spodnja vrsta
    for i=1:n
        b[indeks(i, 1, n)] += -s[i]
        b[indeks(i, m, n)] += -z[i]
    end
    for j=1:m
        b[indeks(1, j, n)] += -l[j]
        b[indeks(n, j, n)] += -d[j]
    end
    return b
end

# definiraj podatkovno strukturo za robni problem na kvadratu. 
# Podatkovna struktura naj vsebuje operator, meje pravokotnika in
# vektor funkcij za vsako stranico posebej 
"""
    RobniProblemPravokotnik(operator, ((a, b), (c, d)), [f_s, f_d, f_z, f_l])

Definiraj robni problem za enačbo z danim diferencialnim operatorjem
```math
\\mathcal{L} u(x,y) = 0
```
na pravokotniku ``[a, b]\\times[c, d]``, kjer so vrednosti na robu podane s 
funkcijami ``u(x, c) = f_s(x)``, ``u(b, y) = f_d(y)``, 
``u(x, d) = f_z(x)`` in ``u(a, y) = f_l(y)``. 
"""
struct RobniProblemPravokotnik
    operator # operator
    meje # oglišča pravokotnika
    rp # funkcije na robu 
end

"""
    Z, x, y = resi(::RobniProglemPravokotnik, h=0.1)

Izračunaj približek za rešitev robnega problema za operator 
z metodo deljenih diferenc.

# Rezultat
- `Z::Matrix` je matrika vrednosti rešitve v notranjosti in na robu.
- `x::Vector` je vektor vrednosti abscise
- `y::Vector` je vektor vrednosti ordinate

# Primer

```julia
using Plots
robni_problem = RobniProblemPravokotnik(
    LaplaceovOperator{2},
    ((0, pi), (0, pi)),
    [sin, y->0, sin, y->0] 
)
Z, x, y = resi(robni_problem)
surface(x, y, Z)
```
"""
function resi(robni_problem::RobniProblemPravokotnik, h = 0.1)
    (a, b), (c, d) = robni_problem.meje
    nx = Int(round((b - a)/h)) - 1 # število notranjih točk
    ny = Int(round((d - c)/h)) - 1
    Z = zeros(nx + 2, ny + 2)
    x = LinRange(a, b, nx + 2)
    y = LinRange(c, d, ny + 2)
    f_s, f_d, f_z, f_l = robni_problem.rp
    Z[:, 1] = f_s.(x)
    Z[end, :] = f_d.(y)
    Z[:, end] = f_z.(x)
    Z[1, :] = f_l.(y)
    b = desne_strani(Z[2:end-1, 1], Z[2:end-1, end],
                     Z[1, 2:end-1], Z[end, 2:end-1], robni_problem.operator)
    L = matrika(nx, ny, robni_problem.operator)
    res = L\b
    Z[2:end-1, 2:end-1] = reshape(res, nx, ny)
    return Z', x, y
end
end # module
