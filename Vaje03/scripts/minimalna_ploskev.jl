using Vaje03

using Plots
# Definiramo robni problem
robni_problem = RobniProblemPravokotnik(
    LaplaceovOperator(2),
    ((0, pi), (0, pi)),
    [sin, y->0, sin, y->0] 
)
# poiščemo rešitev robnega problema
Z, x, y = resi(robni_problem)
# Vizualiziramo rešitev
surface(x, y, Z)