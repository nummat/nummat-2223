module Vaje06

using LinearAlgebra

export potencna, inv_potencna

"""
    v, λ = potencna(A, x0, σ)

Izračunaj lastni vektor za lastno vrednost matrike A, ki je najbolj oddaljena
od vrednosti σ. Lastni vektor izračunamo s potenčno metodo z začetnim približkom `x0`
Funkcija vrne približka za lastni vektor `v` in lastno vrednost 
`λ`.
"""
function potencna(A, x0, σ = 0; maxit = 100, tol = 1e-5)
    x = x0
    # generiraj zaporedje približkov A*x/||A*x||
    for i=1:maxit
        x = A*x0 - σ*x0
        # alternativa Rayleigh kvocientu
        max_index = argmax(abs.(x)) 
        λ = x[max_index]/x0[max_index] # kvocient iste komponente za Ax in x mora biti enak λ
        # Ponovno pomnožimo z A, da se predznaki pokrajšajo
        x = A*x
        x = x/abs(x[max_index])
        x = x - σ*x #premik
        # ko je konvergenčni pogoj izponjen, ustavi iteracijo
        if norm(x - x0, Inf) < tol
            # vrni lastni vektor v in lastno vrednost λ
            return x, λ
        end
        x0 = x
    end
    throw("Iteracija ne konvergira")
end

"""
    v, λ = inv_potencna(A, x0, σ)

Izračunaj lastni vektor za lastno vrednost matrike A, ki je najbližje
vrednosti σ. Približke poiščemo z inverzno potenčno metodo z začetnim približkom `x0`. 
Funkcija vrne približka za lastni vektor `v` in lastno vrednost `λ`.
"""
function inv_potencna(A, x0, σ = 0; maxit = 100, tol = 1e-5)
    x = x0
    # namesto inverza uporabimo
    lu_razcep = lu(A - σ*I) 
    # generiraj zaporedje približkov A^(-1)*x/||A^(-1)*x||
    for i=1:maxit
        x = lu_razcep \ x0 # reši sistem Ax = x0 z LU razcepom (ekvivalentno množenju z A^(-1))
        # alternativa Rayleigh kvocientu
        max_index = argmax(abs.(x)) 
        λ = x0[max_index]/x[max_index] + σ # kvocient iste komponente za Ax in x mora biti enak λ
        # Ponovno pomnožimo z A, da se predznaki pokrajšajo
        x = lu_razcep \ x # reši sistem Ax = x0 z LU razcepom
        x = x/abs(x[max_index])
        # ko je konvergenčni pogoj izponjen, ustavi iteracijo
        if norm(x - x0, Inf) < tol
            # vrni lastni vektor v in lastno vrednost λ
            return x, λ
        end
        x0 = x
    end
    throw("Iteracija ne konvergira")
end

"""
    v, λ = inv_potencna(A, σ, k)

Izračunaj `k`` lastnih lastni vektorev za lastne vrednosti simetrične matrike A,
ki so najbližje vrednosti σ. Funkcija vrne matriko `k`` lastnih vektorev `v` in
`k`` vekrto lastnih vrednosti `λ`.
"""
function inv_potencna(A, σ, k)
end

"""
    G = UniGraph(povezave)

Ustvari nov neusmerjen graf z danimi povezavami.
"""
struct UniGraph
    povezave::Vector{Tuple{Integer, Integer}}
end

"""
    L = laplace(G::UniGraph)

Izračunaj Laplaceovo matriko za dani graf `G`.
"""
function laplace(g::UniGraph)
end

"""
    f, c = fiedler_vector(G)

Izračunaj Fiedlerjev vektor in algebraično povezljivostno število 
za dani neusmerjen graf `G`.
"""
function fiedler_vector(G::UniGraph)
end

end # module