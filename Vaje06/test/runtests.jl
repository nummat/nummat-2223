using Test, Vaje06, LinearAlgebra

Q = [2 2 1; -2 1 2; 1 -2 2]'
A = Q * diagm([1, 2, 3]) / Q

@testset "Potenčna metoda" begin
    v, λ = potencna(A, 0.0)
    @test v * sign(v[1]) ≈ normalize(Q[:, 3])
    @test λ ≈ 3
end

@testset "Inverzna potenčna metoda" begin
    v, λ = inv_potencna(A, 2.1)
    @test v * sign(v[3]) ≈ normalize(Q[:, 2])
    @test λ ≈ 2

    v, λ = inv_potencna(A, 0, 2)
    @test λ[1] ≈ 1
    @test λ[2] ≈ 2
    @test v[:, 1]*sign(v[1, 1]) ≈ normalize(Q[:, 1])
end