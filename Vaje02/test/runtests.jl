using Test, Vaje02

@testset "Množenje tridiagonalne matrike" begin
    # |3 6 0|
    # |1 4 7|
    # |0 2 5| 
    T = Tridiag([1, 2], [3, 4, 5], [6, 7])
    y = T * [1, 2, 3]
    @test y[1] ≈ 15
    @test y[2] ≈ 30
    @test y[3] ≈ 19
end

@testset "LU razcep tridiagonalne matrike" begin
    # uporabiti moramo decimalno verzijo, sicer pride do napake
    T = Tridiag([1.0, 1, 1], [2.0, 2, 2, 2], [1.0, 1, 1])
    L, U = lu(T)
    @test L.s ≈ [0.5, 2/3, 3/4]
    @test U.d ≈ [2, 1.5, 4/3, 5/4]
    @test U.z ≈ T.z
    @test L.d ≈ ones(4)
end