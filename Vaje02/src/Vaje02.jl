module Vaje02

export Tridiag, lu

"""
    Tridiag(s, d, z)

Podatkovni tip za tridiagonalno matriko.
"""
struct Tridiag
    s
    d
    z
end

import Base.*

"""
    T::Tridiag*x

Izračunaj produkt tridiagonalne matrike `T` z vektorjem `x`
"""
function *(T::Tridiag, x)
    y = zero(x) # začetna rešitev
    y[1] = T.d[1] * x[1] + T.z[1] * x[2]
    n = length(x)
    for i=2:n-1
        y[i] = T.s[i-1]*x[i-1] + T.d[i] * x[i] + T.z[i] * x[i+1]
    end
    y[n] = T.s[n-1]*x[n-1] + T.d[n] * x[n]
    return y
end

import Base.length

length(T::Tridiag) = length(T.d)

"""
    L, U = lu(T::Tridiag)

Izračunaj LU razcep brez pivotiranja za tridiagonalno matriko `T`. `L` je spodnje
trikotna tridiagonalna matrika z 1 na diagonali in `U` je zgornjetirkotna tridiagonalna
matrika.  
"""
function lu(T::Tridiag)
    n = length(T)
    l = zero(T.s) # spodnja diagonala L
    u = copy(T.d) # glavna diagonala U
    for i=2:n
        l[i-1] = T.s[i-1]/u[i-1]
        u[i] = u[i] - l[i-1] * T.z[i-1] 
    end
    L = Tridiag(l, ones(n), zeros(n))
    U = Tridiag(zeros(n-1), u, T.z)
    return L, U
end


end # module
