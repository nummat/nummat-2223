# Primer reševanja linearnega sistema v julii
# 2x + y - z = 1
# x -2y + z = 2
# y - z = 3
A = [2 1 -1; 1 -2 1; 0 1 -1] # matrika sistema
b = [1, 2, 3] # vektor desnih strani

# rešitev sistema Ax = b, dobimo z operatorjem \
x = A\b

using LinearAlgebra

# preiskus
norm(A*x - b)

# Pozor! Vgrajen operator \ skriva algoritem za reševanje sistemov

# Reševanje sistema z LU razcepom
# A x = b <=> LU x = b <=> L y = b in U x = y
F = lu(A) 
x = F.U\(F.L\b)
typeof(F)
x = F\b

# Razlika med \, U\L\b in F\b?
# \ vsakokrat izvede Gaussovo eliminacijo (o(n^3)),
# medtem ko U\L\b izvede le direktno in obratno vstavljanje (o(n^2))