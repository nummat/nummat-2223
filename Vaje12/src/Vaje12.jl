module Vaje12

export quad2d, ndquad

"""
    I = quad2d(fun, x0, w)

Izračunaj približek za dvojni integral funkcije `fun(x, y)` na kocki `[a, b]^2` s 
kvadraturno formulo za integral na `[a, b]` z vozlišči `x0` in utežmi `w`
"""
function quad2d(fun, x0, w)
    n = length(x0)
    I = 0
    for i = 1:n
        for j = 1:n
            I += fun(x0[i], x0[j])*w[i]*w[j]
        end
    end
    return I
end

"""
    I = ndquad(f, x0, utezi, d)

izračuna integral funkcije `f` na d-dimenzionalni kocki ``[a,b]^d``
z večkratno uporabo enodimenzionalne kvadrature za integral na 
intervalu ``[a,b]``, ki je podana z utežmi `utezi` in vozlišči `x0`.

# Primer
```jldoctest
julia> f(x) = x[1] + x[2]; #f(x,y)=x+1;
julia> utezi = [1,1]; x0 = [0.5, 1.5]; #sestavljeno sredinsko pravilo
julia> ndquad(f, x0, utezi, 2)
8.0
``` 
"""
function ndquad(f, x0, utezi, d)
    n = length(x0)
    multi_index = [1 for i=1:d]
    I = 0.0
    x = view(x0, multi_index)
    w = view(utezi, multi_index)
    for i=1:n^d
        delta = f(x)*prod(w)
        I += delta
        next_multi_index!(multi_index, n)
    end
    return I
end

function next_multi_index!(multi_index, n)
    d = length(multi_index)
    for i=1:d
        if multi_index[i] < n
            multi_index[i] += 1
            return
        else
            multi_index[i] = 1
        end
    end
end

end # module
