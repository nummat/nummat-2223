# Povprečna razdalja med dvema slučajnima številoma na [-1, 1]
using Vaje11, Vaje12

x0, w = gauss_legendre_rule(3)

# funkcija razdalje med dvema številoma
f(x, y) = abs(y - x)
p(x, y) = 0.25

fun(x, y) = f(x, y) * p(x, y)

I = quad2d(fun, x0, w) 

x0, w = gauss_legendre_rule(4)

I = quad2d(fun, x0, w)

x0, w = gauss_legendre_rule(7)

I = quad2d(fun, x0, w)

x0, w = gauss_legendre_rule(8)

I = quad2d(fun, x0, w)

priblizki = []
for i=1:20
    x0, w = gauss_legendre_rule(i)
    I = quad2d(fun, x0, w)
    append!(priblizki, I)
end 
napaka = priblizki .- priblizki[end]


# Povprečna razdalja med dvema točkama v kvadratu [-1, 1]x[-1, 1]

# funkcija razdalje pomnožena z gostoto verjetnosi
fun(x) = sqrt((x[3] - x[1])^2 + (x[4] - x[2])^2)*(0.5^4)

x0, w = gauss_legendre_rule(100)
I = ndquad(fun, x0, w, 4)

multi_index = [3,3,1,2]
Vaje12.next_multi_index!(multi_index, 3)
multi_index