module Vaje05
using HTTP
greet() = print("Hello World!")

"""
    t, co2 = get_co2_data(url)

Prenesi podatke o koncentraciji CO2 in jih prevedi v tabelo vrednosti.
"""
function get_co2_data(url="https://gml.noaa.gov/aftp/data/trace_gases/co2/in-situ/surface/mlo/co2_mlo_surface-insitu_1_ccgg_DailyData.txt")
    response = HTTP.get(url)
    lines = split(String(response.body), "\n")
    filter!(l->l[1]!='#', lines)
    lines = strip.(lines)
    data = [split(line, r"\s+") for line in lines]
    data = [[parse(Float64, x) for x in line] for line in data]
    t = [l[4] for l in data]
    co2 = [l[5] for l in data]
    t, co2
end

end # module
