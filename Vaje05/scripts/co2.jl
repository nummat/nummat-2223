using Vaje05
using Plots

t, co2 = Vaje05.get_co2_data()
# narišemo podatke na graf
scatter(t, co2, title="Atmosferski CO2 na Mauna Loa",
        xlabel="leto", ylabel="parts per milion (ppm)", label="co2",
        markersize=1)
