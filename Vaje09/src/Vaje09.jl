module Vaje09

export Dual, DualNumber, odvod

"""
    x_dual = DualNumber(x, dx)
    
Podatkovni tip dualnih števil oblike `x + dx⋅ε` z dualno enoto `ε`, ki zadošča enačbi
`ε^2 = 0`.
"""
struct DualNumber <: Number
    x # vrednost funkcije v dani točki
    dx # vrednost odvoda v dani točki
end

import Base:+, -, *, /, ^, isless, show, promote_rule, convert

"Predstavi dualna števila kot x + yε."
show(io::IO, a::DualNumber) = print(io, a.x, " + ", a.dx, "ε")

# Pretvori števila v dualna števila
convert(::Type{DualNumber}, x::Real) = DualNumber(x, zero(x))
# Poskrbi, da se števila pretvorijo v dualna števila, ko je to potrebno
promote_rule(::Type{DualNumber}, ::Type{<:Number}) = DualNumber

# Osnovna aritmetika
+(a::DualNumber, b::DualNumber) = DualNumber(a.x + b.x, a.dx .+ b.dx) # odvajanje vsote
-(a::DualNumber, b::DualNumber) = DualNumber(a.x - b.x, a.dx .- b.dx) # odvajanje razlike
*(a::DualNumber, b::DualNumber) = DualNumber(a.x * b.x, a.dx * b.x .+ a.x * b.dx) # odvajanje produkta
/(a::DualNumber, b::DualNumber) = DualNumber(a.x / b.x, (a.dx * b.x .- a.x * b.dx) / (b.x^2)) # odvod kvocienta

"""
    y = odvod(f<:Function, x)

Izračunaj vrednost odvoda funkcije `f` v točki `x`.
"""
odvod(f, x) = f(DualNumber(x, 1)).dx


"""
    x = spremenljivka(v::Vector{T})

Spremeni vektor v vektorsko spremenljivko števil tipa `Dual`.
"""

"""
    grad = gradient(f, x::Vector)

Izračunaj gradient funkcije vektorske spremenljivke `f` v točki `x`.

## Primer
```jldoctest
f(x) = x[1]^2 + x[2]^2
gradient(f, [1, 2])
[2, 4]
```
"""
 

end # module
