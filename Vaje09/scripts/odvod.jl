using Vaje09

# primer odvoda funkcije
f(x) = x*(1-x) # x-x^2
df(x) = 1 - 2x
odvod(f, 1.1)
df(1.1)

# odvod funkcije podane s programom
function koren(x)
    y = 1 + (x - 1)/2 # en člen Taylorjeve vrste
    for i=1:10
        y = (y + x/y)/2
    end
    return y
end

koren(2)^2

odvod(koren, 2)
# prava vrednost
1/sqrt(2)/2