using Vaje09

a = DualNumber(1, [1, 2, 3])
a + 1

f(x, y, z) = x*(y + z) + z*z

f(DualNumber(1, [1, 0, 0]), DualNumber(2, [0, 1, 0]), DualNumber(3, [0, 0, 1]))