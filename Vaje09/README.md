# Vaja 09 - Avtomatsko odvajanje

Posvetili se bomo [avtomatskemu odvajanju](https://en.wikipedia.org/wiki/Automatic_differentiation) računalniških programov.

## Naloga

1. Implementiraj podatkovni tip `DualNumber` za [dualna števila](https://en.wikipedia.org/wiki/Dual_number).
2. Implementiraj osnovno aritmetiko za dualna števila.
3. Implementiraj funckijo `odvod(f, x)`, ki izračuna vrednost odvoda funkcije `f` v točki `x`.

## Napredna naloga

1. Implementiraj podatkovni tip `Dual`, ki omogoča hranjenje odvodov po več spremenljivkah hkrati.
2. Implementiraj osnovno aritmetiko za podatkovni tip `Dual`.
3. Implementiraj funkciji `gradient(f, x)` in `hessian(f, x)`, ki izračunata vrednost gradienta in Hessejeve matrike za funkcijo več spremenljivk `f`.
4. Implementiraj funkcijo `jacobian(f, x)` za vektorsko funkcijo več spremenljivk.