module Vaje04

using Vaje03
using LinearAlgebra

export resi_iter, korak_gs, korak_jacobi, iteracija

"""
    U1 = korak_jacobi(L, U0)

Izvede en korak Jacobijeve iteracije pri reševanju enačb

```math
u_{i-1,j}+u_{i,j-1} - 4u_{ij} + u_{i+1,j}+u_{i,j+1} = 0.
``` 

# Parametri 
 - `L::LaplaceovOperator{2}``
 - `U0::Matrix` matika vrednosti ``u_{ij}``
 - `metoda::Jacobi`

# Rezultat 
 - `U1::Matrix` vrednost matrike ``U`` v naslednji iteraciji
# Primer
```jldoctest
julia> korak_jacobi(LaplaceovOperator{2}(), [1. 1 1; 2  0 3; 1 4 1])
3×3 Array{Float64,2}:
 1.0  1.0  1.0
 2.0  2.5  3.0
 1.0  4.0  1.0
```
"""
function korak_jacobi(L::LaplaceovOperator{2}, U0)
  U = copy(U0)
  n, m = size(U)
  notranji_indeksi = [(i,j) for i=2:n-1, j=2:m-1]
  for (i,j) in notranji_indeksi
    U[i, j] = (U0[i-1, j] + U0[i, j-1] + U0[i+1, j] + U0[i, j+1])/4
  end
  return U
end
"""
    U1 = korak_gs(L, U0)

Izvedi en korak Jacobijeve iteracije pri reševanju Laplaceove enačbe
na pravokotniku.

# Parametri 
- `L::LaplaceovOperator{2}``
- `U0::Matrix` matika vrednosti ``u_{ij}``
- `metoda::Jacobi`

# Rezultat 
- `U1::Matrix` vrednost matrike ``U`` v naslednji iteraciji
# Primer
```jldoctest
julia> korak_gs(LaplaceovOperator{2}(), [1. 1 1; 2  0 3; 1 4 1])
3×3 Array{Float64,2}:
1.0  1.0  1.0
2.0  2.5  3.0
1.0  4.0  1.0
```
"""
function korak_gs(L::LaplaceovOperator{2}, U0)
 U = copy(U0)
 n, m = size(U)
 notranji_indeksi = [(i,j) for i=2:n-1, j=2:m-1]
 for (i,j) in notranji_indeksi
   U[i, j] = (U[i-1, j] + U[i, j-1] + U[i+1, j] + U[i, j+1])/4
 end
 return U
end

"""
    U = iteracija(korak, U0, robni_indeksi)

Poišče limito rekurzivnega zaporedja ``U_n = korak(U_{n-1})``
z začetnim členom `U0`
# Rezultat
- `U` limita zaporedja
- `it` število korakov iteracija
"""
function iteracija(korak, U0; maxit=1000, tol=1e-10)
  n, m = size(U0)
  it = 0
  U = copy(U0)
  Up = copy(U0)
  for k=1:maxit
    U = korak(Up)
    if norm(U - Up, Inf) < tol
      it = k
      break 
    end
    Up = copy(U)
  end
  return U, it
end

"""
    Z, x, y = resi_iter(robni_problem, h, korak)

Iterativno reši robni problem za PDE na pravokotniku.

# Parameteri

* `robni_problem` podatkovna struktura s podatki o robnem problemu
* `h` resolucija diskretizacije pravokotnika
* `korak` funkcija, ki izračuna nov približek 

# Rezultat

- `Z::Matrix` je matrika vrednosti rešitve v notranjosti in na robu.
- `x::Vector` je vektor vrednosti abscise
- `y::Vector` je vektor vrednosti ordinate
"""
function resi_iter(robni_problem::RobniProblemPravokotnik, h, korak)
    Z, x, y = zacetni_priblizek(robni_problem, h)
    # uporabimo parcialno aplikacijo prvega argumenta, da dobimo funkcijo ene spremenljivke
    korak_iteracije(U) = korak(robni_problem.operator, U)
    Z, it = iteracija(korak_iteracije, Z)
    return Z, x, y
end
"""
    Z, x, y = zacetni_priblizek(robni_problem, h)

Izračunaj začetno matriko za dani robni problem podan z `robni_problem` in s korakom delitve `h`. 
"""
function zacetni_priblizek(robni_problem::RobniProblemPravokotnik, h)
    (a, b), (c, d) = robni_problem.meje
    nx = Int(round((b - a)/h)) - 1 # število notranjih točk
    ny = Int(round((d - c)/h)) - 1
    Z = zeros(nx + 2, ny + 2)
    x = LinRange(a, b, nx + 2)
    y = LinRange(c, d, ny + 2)
    f_s, f_d, f_z, f_l = robni_problem.rp
    Z[:, 1] = f_s.(x)
    Z[end, :] = f_d.(y)
    Z[:, end] = f_z.(x)
    Z[1, :] = f_l.(y)
    return Z, x, y
end

end # module
