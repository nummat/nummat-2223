using Test
using Vaje04, Vaje03

@testset "Korak iteracijskih metod" begin
    U0 = [
        1.0 2 3  4;
        6   0 0  8;
        7   8 9 10;
    ]
    @testset "Korak Jacobi" begin
        U1 = Vaje04.korak_jacobi(LaplaceovOperator(2), U0)
        @test U1[1, :] == U0[1, :]
        @test U1[2, 2] ≈ 4
        @test U1[2, 3] ≈ 5
    end

    @testset "Korak GaussSeidl" begin
        U1 = Vaje04.korak_gs(LaplaceovOperator(2), U0)
        @test U1[2, 2] ≈ 4
        @test U1[2, 3] ≈ 6
    end
end