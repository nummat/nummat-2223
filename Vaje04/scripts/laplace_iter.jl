using Vaje04, Vaje03

using Plots

robni_problem = RobniProblemPravokotnik(
    LaplaceovOperator(2),
    ((0, π), (0, π)),
    [sin, y->0, sin, y->0]
)
Z, x, y = resi_iter(robni_problem, 0.1, korak_gs)
surface(x, y, Z)

# Naredimo še animacijo
L = LaplaceovOperator(2)
U, x, y = Vaje04.zacetni_priblizek(robni_problem, 0.1)
animation = Animation()
for i=1:200
    U = korak_gs(L, U)
    surface(x, y, U, title="Konvergenca Gauss-Seidlove iteracije")
    frame(animation)
end
mp4(animation, "konvergenca.mp4", fps = 10)